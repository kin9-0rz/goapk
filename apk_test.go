package goapk

import (
	// "fmt"

	"strings"
	"testing"
)

var path string
var apk *APK

func init() {
	path = "./testdata/test.apk"
	apk = NewAPK()
	apk.Init(path)
}

func TestNewAPK(t *testing.T) {
	if !strings.Contains(apk.path, "./testdata/test.apk") {
		t.Log("无路径")
		t.Fail()
	}

}

func TestParseAndroidManifest(t *testing.T) {
	apk.ParseAndroidManifest()
	if !strings.Contains(apk.xml, "com.example.hellojni") {
		t.Log("解析AndroidManifest.xml失败！")
		t.Fail()
	}
}

func TestParseDex(t *testing.T) {
	apk.ParseDexs()
	if len(apk.GetDexStrings()) != 17188 {
		t.Log(len(apk.GetDexStrings()))
		t.Fatal()
	}

	// if len(apk.GetClass()) != 817 {
	// 	t.Fatal()
	// }
}

func TestClose(t *testing.T) {
	apk.Close()
}
