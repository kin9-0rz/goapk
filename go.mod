module gitee.com/kin9-0rz/goapk

go 1.15

require (
	github.com/avast/apkparser v0.0.0-20231031113741-3ff9e55acf2f
	github.com/fsnotify/fsnotify v1.5.1 // indirect
	github.com/kin9-0rz/gopcap v0.2.3
	github.com/klauspost/compress v1.17.7 // indirect
	github.com/pelletier/go-toml v1.9.4 // indirect
	github.com/spf13/cast v1.4.1 // indirect
	github.com/spf13/cobra v1.2.1
	github.com/spf13/viper v1.8.1
	golang.org/x/sys v0.0.0-20210906170528-6f6e22806c34 // indirect
	golang.org/x/text v0.3.7 // indirect
	gopkg.in/ini.v1 v1.63.0 // indirect
)
