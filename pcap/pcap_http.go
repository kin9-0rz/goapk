package pcap

// HTTPRequest 存放一次HTTP请求，及其数据。
type HTTPRequest struct {
	URL         string `json:"URL,omitempty"`         // url，不包含 ://
	Method      string `json:"Method,omitempty"`      // GET POST 等请求方法
	RequestData string `json:"RequestData,omitempty"` // 请求数据，包含请求方法、头部、请求体
	ResponeData string `json:"ResponeData,omitempty"` // 响应数据
}

// NewHTTPRequest init
func NewHTTPRequest() HTTPRequest {
	return HTTPRequest{}
}

// SetURL set
func (r *HTTPRequest) SetURL(url string) {
	r.URL = url
}

// GetURL get
func (r *HTTPRequest) GetURL() string {
	return r.URL
}

// SetMethod set
func (r *HTTPRequest) SetMethod(mtd string) {
	r.Method = mtd
}

// GetMethod get
func (r *HTTPRequest) GetMethod() string {
	return r.Method
}

// SetResponeContentType set
func (r *HTTPRequest) SetResponeData(rd string) {
	r.ResponeData = rd
}

// GetResponeData get
func (r *HTTPRequest) GetResponeData() string {
	return r.ResponeData
}

// SetResponeData set
func (r *HTTPRequest) SetRequestData(rd string) {
	r.RequestData = rd
}

// GetRequestData get
func (r *HTTPRequest) GetRequestData() string {
	return r.RequestData
}
