package pcap

import (
	"bytes"
	"strings"
	"testing"
)

func Test_HTTP(t *testing.T) {
	p := NewParser()
	p.Parse("./testdata/test-tcp-http.pcap")

	for _, hr := range p.HTTPRequests {
		if strings.Contains(hr.GetRequestData(), "WusKjuTqWEQ7j0p2o1ZGi6shc4Pczs") {
			if !strings.Contains(hr.GetResponeData(), "JSESSIONID=") {
				t.Fatal()
			}
		}
	}
}

func Test_SMTP(t *testing.T) {
	p := NewParser()
	p.Parse("./testdata/smtp.pcap")

	s := Struct2Json(p.MailInfos.Accounts)
	if !strings.Contains(s, "@vip.163.com") {
		t.Fatal()
	}

	if !strings.Contains(s, "1234qwe") {
		t.Fatal()
	}
}

func Test_DNS_MASS(t *testing.T) {
	p := NewParser()
	p.Parse(`./testdata/test-dns-mass.pcap`)

	s := Struct2Json(p.DNSInfos)
	if !strings.Contains(s, "123.244.92.186") {
		t.Fatal()
	}
}

func Test_DNS_IP6(t *testing.T) {
	p := NewParser()
	p.Parse(`./testdata/test-dns-ip6.pcap`)

	s := Struct2Json(p.DNSInfos)
	if !strings.Contains(s, "2600:1417:") {
		t.Fatal()
	}
}

func Test_DNS_TXT_Record(t *testing.T) {
	p := NewParser()
	p.Parse("./testdata/test_dns_txt.pcap")

	s := Struct2Json(p.DNSInfos)
	if !strings.Contains(s, "stat/v3/udt?pdt") {
		t.Fatal()
	}
}

func Test_TCP_POST(t *testing.T) {
	p := NewParser()
	p.Parse("./testdata/test-tcp-http.pcap")

	s := Struct2Json(p.HTTPRequests)
	if !strings.Contains(s, "api.cp.133155.cn") {
		t.Fatal()
	}

	if !strings.Contains(s, "client.adfeiwo.com:9110/terminal/ad") {
		t.Fatal()
	}
}

func Test_DNS_N(t *testing.T) {
	p := NewParser()
	p.Parse("./testdata/test-dns-n.pcap")

	ips := Struct2Json(p.DNSInfos)
	if !strings.Contains(ips, "220.181.12.16") {
		t.Fatal()
	}

	s := Struct2Json(p.MailInfos)
	if !strings.Contains(s, "@163.com") {
		t.Fatal()
	}

	if !strings.Contains(s, "xyb15530967477") {
		t.Fatal()
	}
}

func Test_DNS_CNAME(t *testing.T) {
	p := NewParser()
	p.Parse("./testdata/test-dns-cname.pcap")

	s := Struct2Json(p.DNSInfos)
	if !strings.Contains(s, "static-plt01.cdndo.com") {
		t.Fatal()
	}

	if !strings.Contains(s, "218.78.186.99") {
		t.Fatal()
	}

	for _, item := range p.HTTPRequests {
		if strings.Contains(item.URL, "sys/mod/sdk") {
			goto PASS
		}
	}
	t.Fatal()
PASS:
}

func Test_Err0(t *testing.T) {
	p := NewParser()
	p.Parse("./testdata/err_0.pcap")

	dis := p.DNSInfos
	flag := true

	for _, di := range dis {
		if di.Domain != "oc.umeng.com" {
			continue
		}

		for _, a := range di.Answers {
			if a.Name == "oc.umeng.com.gds.alibabadns.com" {
				flag = false
				if a.Addr != "203.119.128.55" {
					t.Fatal()
				}
			}
		}
	}

	if flag {
		t.Fatal()
	}
}

func Test_Err1(t *testing.T) {
	p := NewParser()
	p.Parse("./testdata/err_1.pcap")
}

func Test_Err2(t *testing.T) {
	p := NewParser()
	p.Parse("./testdata/err_2.pcap")
}

func Test_Err3(t *testing.T) {
	p := NewParser()
	p.Parse("./testdata/err_3.pcap")
}

func Test_TCP_DATA(t *testing.T) {
	p := NewParser()
	p.Parse("./testdata/test_vwoijgwe213.pcap")

	for _, item := range p.TDatas {
		if bytes.Contains(item.Data, []byte("vwoijgwe213213")) {
			goto PASS
		}
	}

	t.Fatal()

PASS:
}

func Test_DNS_8182(t *testing.T) {
	p := NewParser()
	p.Parse("./testdata/test-dns-0x8182.pcap")

	if len(p.DNSInfos) != 1 {
		t.Fatal()
	}
}

func Test_Err111(t *testing.T) {
	p := NewParser()
	p.Parse("./testdata/err_queries_is_not_1.pcap")
}
