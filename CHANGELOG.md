# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [1.1.5](https://gitee.com/kin9-0rz/goapk/compare/v1.1.4...v1.1.5) (2021-11-02)

### [1.1.4](https://gitee.com/kin9-0rz/goapk/compare/v1.1.3...v1.1.4) (2021-09-08)

### [1.1.3](https://gitee.com/kin9-0rz/goapk/compare/v1.1.2...v1.1.3) (2021-06-07)

### [1.1.2](https://gitee.com/kin9-0rz/goapk/compare/v1.1.1...v1.1.2) (2021-05-11)

### [1.1.1](https://gitee.com/kin9-0rz/goapk/compare/v1.1.0...v1.1.1) (2021-05-11)


### Bug Fixes

* 🐛 url的path解析异常 ([adac6dd](https://gitee.com/kin9-0rz/goapk/commit/adac6ddb645f625814ab095a3c5f5c6151bfde0d))

## 1.1.0 (2021-03-31)


### Features

* 🎸 HTTP请求，数据上下行分离 ([342bf6d](https://gitee.com/kin9-0rz/goapk/commit/342bf6d12afeab39622c4c42f590345cd95bc561))
* 🎸 init ([d60ea0f](https://gitee.com/kin9-0rz/goapk/commit/d60ea0f38c2163e66c168d2fefa3170926249433))


### Bug Fixes

* 🐛 support the case that DNS query failure ([c17ec33](https://gitee.com/kin9-0rz/goapk/commit/c17ec33f98e8d6bc6ee826cebfb742b3890de11f))
* 🐛 修复DNS解析异常 ([c990ad3](https://gitee.com/kin9-0rz/goapk/commit/c990ad3d75d094493dca1a0e9cc7515b47448dae))
