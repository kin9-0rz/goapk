.DEFAULT_GOAL:=help

.PHONY: help deps clean build watch

help:  ## Display this help
	@awk 'BEGIN {FS = ":.*##"; printf "\nUsage:\n  make \033[36m\033[0m\n\nTargets:\n"} /^[a-zA-Z_-]+:.*?##/ { printf "  \033[36m%-10s\033[0m %s\n", $$1, $$2 }' $(MAKEFILE_LIST)

deps:  ## Check dependencies
	$(info Checking and getting dependencies)
	go get -u

clean: ## Cleanup the project folders
	$(info Cleaning up things)
	go clean -i -cache -testcache

test: clean deps ## Test the project
	$(info Testing)
	@go test ./...

build: clean deps ## Build the project
	$(info Building the project)
	go build

install: deps ## Install the project
	$(info Installing the project)
	go install
goapk: deps ## Install the cmd
	$(info Installing the project)
	go install gitee.com/kin9-0rz/goapk/goapk
