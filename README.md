# goapk 

[![Go Reference](https://pkg.go.dev/badge/gitee.com/kin9-0rz/goapk.svg)](https://pkg.go.dev/gitee.com/kin9-0rz/goapk) [![Go Report Card](https://goreportcard.com/badge/gitee.com/kin9-0rz/goapk)](https://goreportcard.com/report/gitee.com/kin9-0rz/goapk)

#### 介绍

A library for parsing apk, dex, pcap file.

#### 安装

```
go get -u gitee.com/kin9-0rz/goapk
```