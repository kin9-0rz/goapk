package elf

import (
	"fmt"
	"os"
	"testing"

	"github.com/avast/apkparser"
)

func TestELF(t *testing.T) {
	path := "../testdata/test.apk"

	apkReader, err := apkparser.OpenZip(path)
	if err != nil {
		fmt.Fprintln(os.Stderr, err)
		return
	}
	defer apkReader.Close()

	so := apkReader.File["lib/armeabi/libtest.so"]
	if so == nil {
		t.Fatal()
	}

	if err := so.Open(); err != nil {
		fmt.Println(fmt.Errorf("Failed to open resources.arsc: %s", err.Error()))
		return
	}
	defer so.Close()

	ep := NewParser()
	ep.Parse(so)

	if len(ep.ImportedLibraries) != 4 {
		t.Fatal()
	}

	if len(ep.ImportedSymbols) != 5 {
		t.Fatal()
	}

	section := ep.File.Section(".dynstr")
	data, _ := section.Data()
	strings := ep.ReadString(data)

	if len(strings) != 68 {
		t.Fatal()
	}

	// for key, value := range strings {
	// 	fmt.Println(key, string(value))
	// 	fmt.Printf("%#x %s\n", key, string(value))
	// }

}
