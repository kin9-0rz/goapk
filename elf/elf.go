package elf

import (
	"bytes"
	"debug/elf"
	"io"
	"io/ioutil"
)

// Parser ELF信息
type Parser struct {
	File              *elf.File
	ImportedLibraries []string
	ImportedSymbols   []elf.ImportedSymbol
}

// NewParser 初始化
func NewParser() *Parser {
	ep := &Parser{}

	return ep
}

// Parse 解析reader
func (e *Parser) Parse(r io.Reader) {
	b, _ := ioutil.ReadAll(r)
	e.File, _ = elf.NewFile(bytes.NewReader(b))
	e.ImportedLibraries, _ = e.File.ImportedLibraries()
	e.ImportedSymbols, _ = e.File.ImportedSymbols()
}

// ParseFile 解析elf文件
func (e *Parser) ParseFile(path string) {

}

// ReadString 从原始数据中找出字符串
func (e *Parser) ReadString(b []byte) map[uint64][]byte {
	var slice [][]byte
	if slice = bytes.Split(b, []byte("\x00")); slice == nil {
		return nil
	}

	strings := make(map[uint64][]byte, len(slice))
	length := uint64(len(slice))

	var offset uint64
	for i := uint64(0); i < length; i++ {
		if len(slice[i]) < 3 {
			continue
		}
		strings[offset] = slice[i]
		// TODO 代码在文件中的位置
		offset += (uint64(len(slice)) + 1)
	}

	return strings
}
