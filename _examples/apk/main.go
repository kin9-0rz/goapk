package main

import (
	"fmt"
	"strings"

	"gitee.com/kin9-0rz/goapk"
)

func main() {
	path := "../../testdata/test.apk"
	apk, err := goapk.NewAPK(path)
	if err != nil {
		fmt.Println(err)
		return
	}
	apk.ParseDexs()
	for i, v := range apk.GetDexStrings() {
		if strings.Contains(v, "hello") {
			fmt.Println(i, v)
		}
	}
}
