package main

import (
	"fmt"

	"gitee.com/kin9-0rz/goapk/pcap"
)

func main() {
	path := "../../pcap/testdata/test_vwoijgwe213.pcap"
	p := pcap.NewParser()
	p.Parse(path)
	fmt.Println(p.DNSInfos)
	fmt.Println(p.IPs)
	fmt.Println(p.TDatas)
	fmt.Println(p.HTTPRequests)
}
