/*
Copyright © 2022 NAME HERE <EMAIL ADDRESS>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cmd

import (
	"bytes"
	"encoding/xml"
	"fmt"
	"os"

	"github.com/avast/apkparser"
	"github.com/spf13/cobra"
)

// manifestCmd represents the manifest command
var manifestCmd = &cobra.Command{
	Use:   "manifest",
	Short: "解析清单",
	Long:  `解析清单`,
	Run: func(cmd *cobra.Command, args []string) {
		if len(args) == 1 {
			f, err := os.Open(args[0])
			if err != nil {
				fmt.Println(err)
				return
			}
			defer f.Close()

			buf := bytes.NewBufferString("")
			enc := xml.NewEncoder(buf)
			err = apkparser.ParseXml(f, enc, nil)
			if err != nil {
				fmt.Fprintln(os.Stderr, err)
				return
			}

			fmt.Println(buf.String())

		} else {
			cmd.Help()
		}
	},
}

func init() {
	rootCmd.AddCommand(manifestCmd)
}
