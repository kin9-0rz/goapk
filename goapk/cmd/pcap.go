/*
Copyright © 2021 NAME HERE <EMAIL ADDRESS>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cmd

import (
	"fmt"

	"gitee.com/kin9-0rz/goapk/pcap"
	"github.com/spf13/cobra"
)

// pcapCmd represents the pcap command
var pcapCmd = &cobra.Command{
	Use:   "pcap",
	Short: "解析pcap文件",
	Long:  `解析pcap文件`,
	Run: func(cmd *cobra.Command, args []string) {
		if len(args) == 1 {
			p := pcap.NewParser()
			p.Parse(args[0])

			for _, item := range p.HTTPRequests {
				fmt.Println(item.GetMethod(), item.GetURL())
			}
		} else {
			cmd.Help()
		}
	},
}

func init() {
	rootCmd.AddCommand(pcapCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// pcapCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// pcapCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}
