/*
Copyright © 2022 NAME HERE <EMAIL ADDRESS>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cmd

import (
	"fmt"

	"gitee.com/kin9-0rz/goapk"
	"github.com/spf13/cobra"
)

// apkCmd represents the apk command
var apkCmd = &cobra.Command{
	Use:   "apk",
	Short: "解析apk",
	Long:  `遍历apk所有的文件`,
	Run: func(cmd *cobra.Command, args []string) {
		if len(args) == 1 {
			apk := goapk.NewAPK()
			defer apk.Close()

			err := apk.Init(args[0])
			if err != nil {
				fmt.Println(err)
			}

			m, err := cmd.Flags().GetBool("manifest")
			if err != nil {
				fmt.Println(err)
				return
			}

			if m {
				fmt.Println(apk.GetAndroidManifest())
			}

			s, err := cmd.Flags().GetBool("string")
			if err != nil {
				fmt.Println(err)
				return
			}

			if s {
				apk.ParseDexs()
				for i, v := range apk.GetDexStrings() {
					fmt.Println(i, v)
				}
			}

		} else {
			cmd.Help()
		}
	},
}

func init() {
	rootCmd.AddCommand(apkCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	apkCmd.PersistentFlags().String("f", "foo", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	apkCmd.Flags().BoolP("manifest", "m", false, "解析AndroidManifest")
	apkCmd.Flags().BoolP("string", "s", false, "解析Dex字符串")
	apkCmd.Flags().String("file", "", "APK文件")

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// apkCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// apkCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}
