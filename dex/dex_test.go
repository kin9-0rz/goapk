package dex

import (
	"fmt"
	"strings"

	"github.com/avast/apkparser"

	// "encoding/binary"
	// "log"
	"os"
	"testing"
	// "io"
	// "bufio"
	// "bytes"
	// "io/ioutil"
	// "strings"
)

func TestNewFile(t *testing.T) {
	path := "../testdata/test.apk"

	apkReader, err := apkparser.OpenZip(path)
	if err != nil {
		fmt.Fprintln(os.Stderr, err)
		return
	}
	defer apkReader.Close()

	dex := apkReader.File["classes.dex"]
	if dex == nil {
		print(os.ErrNotExist)
		return
	}

	if err := dex.Open(); err != nil {
		fmt.Println(fmt.Errorf("Failed to open resources.arsc: %s", err.Error()))
		return
	}
	defer dex.Close()
	dexFile, err := NewFile(dex)
	if err != nil {
		t.Log(err)
		t.Fail()
	}

	// 字符串测试，直接根据索引查找字符串
	size := dexFile.stringIds.size
	if size != 8594 {
		t.Log("字符串解析数量不对")
		t.Fail()
	}

	counter := 0
	for i := 0; i < size; i++ {
		s := dexFile.String(i)
		if strings.Contains(s, "hello") {
			counter++
		}
	}

	if counter != 15 {
		t.Log("包含hello的字符串数量不对")
		t.Fail()
	}

	// 类型测试
	size = dexFile.typeIds.size
	if size != 1262 {
		t.Log("类型数量不对", size)
		t.Fail()
	}
	counter = 0
	for i := 0; i < size; i++ {
		s := dexFile.Type(i)
		if strings.Contains(s, "Ljava/lang/String;") {
			counter++
		}
	}
	if counter != 2 {
		t.Log("包含Ljava/lang/String;的类型数量不对")
		t.Fail()
	}

	dexFile.parseClassDefs()
	if len(dexFile.dexClasses) != 817 {
		t.Fatal()
	}

	for _, item := range dexFile.dexClasses {
		if item.supperClassName != "Landroid/app/Activity;" {
			continue
		}

		if !strings.Contains(item.className, "MainA") {
			continue
		}

		for _, m := range item.data.methods {
			if m.id.name == "onCreate" {
				if m.id.sign != "(Landroid/os/Bundle;)V" {
					t.Fatal()
				}
			}
		}
	}
}
